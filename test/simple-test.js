"use strict";

var test = require('tape'),
	pg = require('pg'),

	conString = "postgres://postgres@localhost/",
	dbName = 'test';

test('Create database', function (t) {

	pg.connect(conString + 'postgres', function(err, client, done) {
		t.notOk(err, 'should connect to master-db without errors');

		client.query('CREATE DATABASE ' + dbName, function() {
			done();
			t.end();
		});
	});
});

test('Create table', function (t) {

	pg.connect(conString + 'postgres', function(err, client, done) {
		t.notOk(err, 'should connect to test-db without errors');

		client.query('DROP TABLE names', function() {

			client.query('CREATE TABLE names ( name text )', function(err) {
				t.notOk(err);

				done();
				t.end();
			});
		});
	});
});

test('Insert values in table', function (t) {

	pg.connect(conString + 'postgres', function(err, client, done) {
		t.notOk(err, 'should connect to test-db without errors');

		client.query("INSERT INTO names (name) VALUES ('marcus')", function(err) {
			t.notOk(err);

			done();
			t.end();
		});
	});
});

test('Selecting names in table', function (t) {

	pg.connect(conString + 'postgres', function(err, client, done) {
		t.notOk(err, 'should connect to test-db without errors');

		client.query("SELECT name FROM names", function(err, result) {
			t.notOk(err);

			t.equal(result.rowCount, 1, 'should return 1 row');
			t.equal(result.rows[0].name, 'marcus', 'should have the name "marcus"');

			done();
			t.end();
		});
	});
});

test('Close the POSTGRES connection.', function (t) {
	pg.end();
	t.end();
});
